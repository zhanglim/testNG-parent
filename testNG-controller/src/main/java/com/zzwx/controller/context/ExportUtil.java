package com.zzwx.controller.context;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.zzwx.common.utils.CommonUtil;

@SuppressWarnings({"rawtypes","unchecked"})
public class ExportUtil {
	/**
	 * 导出数据
	 * 
	 * @param response
	 * @param request
	 * @param title
	 *            文件�? 默认�?:data.xls
	 * @param data
	 * @param propertyAndHeaderss
	 *            //"name:名称,code:编码,descp:描叙,mydate:创建日期"
	 * @param columnWidth
	 *            //行宽�?35px
	 * @throws Exception
	 */
	public static void generateExcel(HttpServletResponse response,
			HttpServletRequest request, String title,  List data,
			String propertyAndHeaderss, Integer columnWidth) throws Exception {
		List<PropertyAndHeader> propertyAndHeaders = PropertyAndHeader
				.getProertyHeaderList(propertyAndHeaderss);
		if (CommonUtil.isEmpty(title)) {
			title = "data.xls";
		}
		response.setContentType("application/x-msdownload;charset=UTF-8");
		response.setHeader("Content-Disposition", "attachment;filename="
				+ URLEncoder.encode(title, "UTF-8"));
		OutputStream out = response.getOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(title);
		if (null == columnWidth) {
			sheet.setDefaultColumnWidth(35);
			sheet.setDefaultRowHeightInPoints((short) 15);
		} else {
			sheet.setDefaultColumnWidth(columnWidth);
		}
		// 创建 单元�? 样式
		HSSFCellStyle style = wb.createCellStyle();
		style.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 生成�?个字�?
		HSSFFont font = wb.createFont();
		font.setColor(HSSFColor.VIOLET.index);
		font.setFontHeightInPoints((short) 10);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 把字体样�? 应用�? cell �?
		style.setFont(font);

		// 生成并设置另�?个样�?
		HSSFCellStyle style2 = wb.createCellStyle();
		style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style2.setFillForegroundColor(HSSFColor.WHITE.index);
		style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		// 生成另一个字�?
		HSSFFont font2 = wb.createFont();
		font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样�?
		style2.setFont(font2);

		// 声明�?个画图的顶级管理�?
		HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
		// 定义注释的大小和位置,详见文档
		HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,
				0, 0, 0, (short) 4, 2, (short) 6, 5));
		// 设置注释内容
		comment.setString(new HSSFRichTextString("可以在POI中添加注释！"));
		// 设置注释作�?�，当鼠标移动到单元格上是可以在状�?�栏中看到该内容.
		comment.setAuthor("leno");
		// 产生表格标题�?
		HSSFRow row = sheet.createRow(0);
		createCell(row, style, propertyAndHeaders);
		for (int i = 0; i < data.size(); i++) {
			HSSFRow hr = sheet.createRow(i + 1);
			Map<String, Object> propertyMap = ObjectUtil.bean2Map(data.get(i));
			if (propertyMap != null) {
				for (int j = 0; j < propertyAndHeaders.size(); j++) {
					HSSFCell c = hr.createCell(j);
					c.setCellStyle(style2);
					Object valueObject = propertyMap.get(propertyAndHeaders
							.get(j).getProperty());
					if (valueObject != null) {
						valueObject = valueObject.toString();
					} else {
						valueObject = "";
					}
					c.setCellValue(valueObject.toString());
				}
			}
		}
		wb.write(out);
		out.flush();
	}

	/**
	 * 为excel 生成单元�?
	 * 
	 * @param row
	 */
	public static void createCell(HSSFRow row, HSSFCellStyle rowStyle,
			List<PropertyAndHeader> propertyAndHeaders) {
		for (int i = 0; i < propertyAndHeaders.size(); i++) {
			HSSFCell c = row.createCell(i);
			c.setCellStyle(rowStyle);
			c.setCellValue(propertyAndHeaders.get(i).getHeader());
		}
	}
}

class PropertyAndHeader {
	String property;
	String header;

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public PropertyAndHeader(String property, String header) {
		super();
		this.property = property;
		this.header = header;
	}

	public PropertyAndHeader() {
	}

	/**
	 *
	 * @param propertyAndHeaders
	 *            "title:标题,name:姓名"
	 * @return
	 */
	public static List<PropertyAndHeader> getProertyHeaderList(
			String propertyAndHeaders) {
		List<PropertyAndHeader> list = new ArrayList<PropertyAndHeader>();
		if (CommonUtil.isNotEmpty(propertyAndHeaders)) {
			String[] propertyAndHeader = propertyAndHeaders.split(",");
			for (String ph : propertyAndHeader) {
				String[] phs = ph.split(":");
				PropertyAndHeader pah = new PropertyAndHeader(phs[0], phs[1]);
				list.add(pah);
			}
		}
		return list;
	}

}