package com.zzwx.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * 请求字符检查过滤器
 * 
 * @author GaoXiang Date: 2016-3-1 下午12:12:36
 */
public class LinkyCharacterFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		// 请求的uri
		HttpServletRequest httpRequest = (HttpServletRequest) req;
		// url
		// String url = httpRequest.getRequestURL().toString();
		
		// 请求方法
		// String method = httpRequest.getMethod();
		
		// Ip
		// String ip = httpRequest.getLocalAddr();
		boolean doFilter = true;
		 
		if (doFilter) {
 			LinkyRequest wrapRequest = new LinkyRequest(httpRequest, httpRequest.getParameterMap());
 			
 			// 以上为该请求执行前的操作
			chain.doFilter(wrapRequest, res);
			// 以下为该请求执行后的操作
			
		} else {
			chain.doFilter(req, res);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
