<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglibs.jsp"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>无标题文档</title>
  <link href="${ctx}/css/style.css" rel="stylesheet" type="text/css" />
  <script language="JavaScript" src="${ctx}/js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript">
    $(function(){
      //顶部导航切换
      $(".nav li a").click(function(){
        $(".nav li a.selected").removeClass("selected")
        $(this).addClass("selected");
      })
    })

    function setLeftNavValue(parentId,name){
      top.leftFrame.initLeftNav(parentId,name);
    }


  </script>



</head>
<body style="background:url(${ctx}/images/topbg.gif) repeat-x;">

<div class="topleft">
  <a href="main.html" target="_parent"><img src="${ctx}/images/logo.png" title="系统首页" /></a>
</div>

<ul class="nav">
  <li><a href="${ctx}/itemManager/index" onclick="javascript:setLeftNavValue(0,'系统管理');"  class="selected" target="rightFrame"><img src="${ctx}/images/icon02.png" title="系统管理" /><h2>系统管理</h2></a></li>
  <li><a href="${ctx}/itemManager/index" onclick="javascript:setLeftNavValue(1,'会员管理');"  target="rightFrame"><img src="${ctx}/images/icon03.png" title="会员管理" /><h2>会员管理</h2></a></li>
  <li><a href="${ctx}/itemManager/index" onclick="javascript:setLeftNavValue(2,'商户商品管理');"  target="rightFrame"><img src="${ctx}/images/icon04.png" title="商户商品管理" /><h2>商户商品管理</h2></a></li>
  <li><a href="${ctx}/itemManager/index" onclick="javascript:setLeftNavValue(3,'优惠管理');"  target="rightFrame"><img src="${ctx}/images/icon05.png" title="优惠管理" /><h2>优惠管理</h2></a></li>
</ul>

<div class="topright">
  <ul>
     <li><span><img src="${ctx}/images/help.png" title="帮助"  class="helpimg"/></span><a href="#">帮助</a></li>
     <li><a href="#">关于</a></li>
    <li><a  href="${ctx}/logout" target="_parent"><b>退  出</b></a></li>
  </ul>

  <div class="user">
    <span>admin</span>
    <%-- <i>消息</i>
     <b>5</b>--%>
   </div>

</div>

</body>
</html>
