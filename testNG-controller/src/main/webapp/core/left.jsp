<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>无标题文档</title>
  <script src="${ctx}/js/jquery-1.11.3.min.js" type="text/javascript" charset="UTF-8"></script>
  <link href="${ctx}/css/style.css" rel="stylesheet" type="text/css" />

</head>
<body style="background:#f0f9fd;">
<div class="lefttop"><span></span>导航菜单</div>
<dl class="leftmenu" id="leftmenu"></dl>

<script type="text/javascript">
  $(function(){
    initLeftNav(0,'系统管理');
    //导航切换
    switchNav();
  });

  /*function initLeftNav(parentId){
    var navStr = "";
    if(parentId && parentId>0){
      $('#leftmenu').html("<dd><img alt='' src='${ctx}/images/blue/loading.gif'>数据加载中...</dd>");
      var i=0;
      $.ajax({
        url:"${ctx}/roleAuth/getRoleAuthTree",
        type:"POST",
        data:{'menuType':0,'parentId':parentId},
        dataType:"json",
        async: false,
        success:function(data){
          if(data && data.length>0){
            for(d in data){
              var nav = data[d];
              if(nav){
                navStr += "<dd>";
                navStr += "<div class='title'><span><img src='${ctx}/images/blue/leftico01.png'/></span>"+nav.name+"</div>";
                var children = nav.children;
                if(children && children.length>0){
                  navStr += "<ul class='menuson'>";
                  for(c in children){
                    var subNav = children[c];
                    if(subNav){
                      if(i==0){
                        parent.topFrame.initIndexPage(parentId,subNav.url);
                      }
                      navStr += " <li><cite></cite><a href='${ctx}/"+subNav.url+"' target='rightFrame'>"+subNav.name+"</a><i></i></li>";
                    }
                    i++;
                  }
                  navStr += "</ul>";
                }
                navStr += "</dd>";
              }
            }
          }
        },error:function(data){
          $.messager.alert('提示',data,'error');
        }
      });
    }else{
    }
    $('#leftmenu').html(navStr);
    switchNav();
  }*/
  function initLeftNav(parentId,name){
    var navStr = "";
      navStr += "<dd>";
      navStr += "<div class='title'><span><img src='${ctx}/images/leftico01.png'/></span>"+name+"</div>";
      navStr += "<ul class='menuson'>";
      if(parentId==0){
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>账户管理</a><i></i></li>";
      }else if(parentId==1){
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾生活背景设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>轮换图设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
      }else if(parentId==2){
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾生活背景设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>轮换图设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
      }else if(parentId==3){
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾生活背景设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>轮换图设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
        navStr += " <li><cite></cite><a href='${ctx}/test/toTest?dd=2015-1-1 ' target='rightFrame'>漾红包设定</a><i></i></li>";
      }

      navStr += "</ul>";
      navStr += "</dd>";
    $('#leftmenu').html(navStr);
  }

  function switchNav(){
    $(".menuson li").click(function(){
      $(".menuson li.active").removeClass("active")
      $(this).addClass("active");
    });
    $('.title').click(function(){
      var $ul = $(this).next('ul');
      $('dd').find('ul').slideUp();
      if($ul.is(':visible')){
        $(this).next('ul').slideUp();
      }else{
        $(this).next('ul').slideDown();
      }
    });
  }

</script>
</body>
</html>
