<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>菜单列表</title>
</head>
<body>
    <table>
        <tr>
            <td>ID</td>
            <td>菜单名</td>
            <td>菜单路径</td>
            <td>操作</td>
        </tr>
        <c:forEach items="${menus}" var="menus">
            <tr>
                <td>${menus.id}</td>
                <td>${menus.menuName}</td>
                <td>${menus.menuUrl}</td>
                <td><a href="/user/findMenus/${menus.id}">查看详情</a> </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
