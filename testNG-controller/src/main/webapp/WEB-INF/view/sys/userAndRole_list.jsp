<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户列表</title>
</head>
<body>
    <table>
        <tr>
            <td>用户ID</td>
            <td>用户名</td>
            <td>所属角色</td>
        </tr>
        <c:forEach items="${data}" var="data">
            <tr>
                <td>${data.userId}</td>
                <td>${data.userName}</td>
                <td>${data.roleName}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
