<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../core/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>用户登录</title>
	<script type="text/javascript" src="${ctx}/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/cloud.js"></script>

</head>

<body>
    <div >
	    <form action="/zzwxIndex/login" method="post">
		    <input type="text" name="userName" value="${userName }" placeholder="请输入用户名"/><br/>
		    <input type="password" name="password" placeholder="请输入密码"/><br/>
		    ${errorMsg}
			<br/>
			<input type="submit" value="登录"/>
	    </form>
	    </div>
    </div>
</body>
</html>