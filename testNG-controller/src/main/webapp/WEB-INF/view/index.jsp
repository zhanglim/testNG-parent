<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>主页面</title>
</head>
<body>
    <h1>这是主页面 <shiro:principal/> </h1>
    <shiro:hasPermission name="/user/findUsers">
        <a href="/user/findUsers">用户列表</a>
    </shiro:hasPermission>

    <shiro:hasPermission name="/user/findMenus">
        <a href="user/findMenus">菜单列表</a>
    </shiro:hasPermission>

    <a href="user/getUserAndRole">用户角色列表</a>

</body>
</html>
