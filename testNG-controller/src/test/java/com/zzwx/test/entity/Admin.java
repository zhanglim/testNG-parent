package com.zzwx.test.entity;

public class Admin {

	private int id;

	private String userName;

	private String password;

	private boolean gender;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Admin [id=" + id + ", userName=" + userName + ", password="
				+ password + ", gender=" + gender + "]";
	}

}
