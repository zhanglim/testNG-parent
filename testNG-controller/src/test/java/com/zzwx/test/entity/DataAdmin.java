package com.zzwx.test.entity;

import org.testng.annotations.DataProvider;

public class DataAdmin {

	@DataProvider(name = "admin")
	public static Object[][] admin() {
		return new Object[][] { { 10, "admin10", "adminPwd10", false },
				{ 11, "admin11", "adminPwd11", false } };
	}

}
