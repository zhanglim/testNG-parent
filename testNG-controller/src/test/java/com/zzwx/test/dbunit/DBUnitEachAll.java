package com.zzwx.test.dbunit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.sql.Connection;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.zzwx.test.dbunit.base.DBUnitBase;

/**
 * @author Roger
 * @desc 添加测试数据并备份测试前数据库所有数据到一个备份文件中
 */
public class DBUnitEachAll extends DBUnitBase {

	/**
	 * 添加测试数据并备份数据
	 * 
	 * @param fileName
	 *            文件名
	 * @param tableNames
	 *            表名集合
	 */
	@SuppressWarnings("deprecation")
	public void setUpBackupEachAll(String fileName, String... tableNames) {
		// JDBC数据库连接
		Connection conn = null;
		// DBUnit数据库连接
		IDatabaseConnection connection = null;
		try {
			conn = getConnection();
			// 获得DB连接
			connection = new DatabaseConnection(conn);
			// 备份数据库测试之前的数据
			backupDataEachAll(tableNames);
			// 准备数据的读入
			IDataSet dataSet = new FlatXmlDataSet(new FileInputStream(
					testDataPath + fileName));
			connection.createDataSet(new String[] {});
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeCon();
		}
	}

	/**
	 * 通过表名将数据库中的数据备份到一个xml备份文件中
	 * 
	 * @param tableNames
	 *            表名集合
	 * @throws Exception
	 */
	public void backupDataEachAll(String... tableNames) throws Exception {
		try {
			conn = getConnection();
			// 获得DB连接
			connection = new DatabaseConnection(conn);
			
			QueryDataSet backupDataSet = new QueryDataSet(connection);

			if (null != tableNames && tableNames.length > 0) {
				for (String tableName : tableNames) {
					backupDataSet.addTable(tableName);
				}
			}
			// 设置备份文件路径
			file = new File(backupDataPath + "each_all_data_back.xml");
			FlatXmlDataSet.write(backupDataSet, new FileWriter(file), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeCon();
		}
	}

	/**
	 * 还原备份的数据
	 */
	public void recoverBackupEachAll() {
		if(null != file){
			super.recoverData(file);
		}
	}

}
