package com.zzwx.test.junit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.zzwx.test.testcalss.MyTestClass;

public class TestJunitSimple {

	private MyTestClass myTest;
	
	@Before
	public void setUp(){
		System.out.println("junit before !");
		myTest = new MyTestClass();
	}
	
	@Test
	public void testSimple(){
		System.out.println("this is simple junit test !");
		String str = myTest.print("simple junit test");
		Assert.assertNotNull(str);
	}
	
	@After
	public void down(){
		System.out.println("junit after !");
	}
	
	
}
