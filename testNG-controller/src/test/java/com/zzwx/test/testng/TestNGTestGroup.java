package com.zzwx.test.testng;

import org.testng.annotations.Test;

/**
 * @author Roger
 * @desc 测试分组
 */
public class TestNGTestGroup {

	@Test(groups = { "adminGroup" })
	public void testGroup1() {
		System.out.println(this.getClassName() + " : adminGroup 1");
	}

	@Test(groups = { "adminGroup" })
	public void testGroup11() {
		System.out.println(this.getClassName() + " : adminGroup 2");
	}

	@Test(groups = { "userGroup" })
	public void testGroup2() {
		System.out.println(this.getClassName() + " : userGroup 1");
	}

	@Test(groups = { "userGroup" })
	public void testGroup22() {
		System.out.println(this.getClassName() + " : userGroup 2");
	}

	public String getClassName() {
		return this.getClass().getSimpleName();
	}
}
