package com.zzwx.test.testng;

import org.testng.annotations.Test;

/**
 * @author Roger
 * @desc TestNG依赖测试
 */
public class TestNGTestDepends {

	public String getClassName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * @alwaysRun = true -- > 依赖源失败后继续执行，方法不被标记为Skips
	 * @alwaysRun = false -- > 依赖源失败后继续执行，方法被标记为Skips，
	 */
	@Test(alwaysRun = false, dependsOnMethods = { "testNGTestDepends" })
	public void testNGTestBy() {
		System.out.println(this.getClassName() + " - testNGTestBy");
	}

	@Test
	public void testNGTestDepends() {
		System.out.println(this.getClassName() + " - testNGTestDepends");
	}
}
