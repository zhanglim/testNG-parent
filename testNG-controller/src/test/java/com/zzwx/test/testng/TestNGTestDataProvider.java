package com.zzwx.test.testng;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.zzwx.test.entity.Admin;
import com.zzwx.test.entity.DataAdmin;

/**
 * @author Roger
 * @desc 通过DataProvider传递参数
 */
public class TestNGTestDataProvider {

	public String getClassName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * 参数定义格式必须是一个二维数组，否则抛异常
	 * 
	 * @return Object[][]
	 */
	@DataProvider(name = "user")
	public Object[][] user() {
		return new Object[][] { { "line", "linePassword" },
				{ "tomcat", "tomcatPassword" } };
	}

	@Test(dataProvider = "user", enabled = true)
	public void testNGTestDataProvider(String name, String password) {
		System.out.println(this.getClassName() + "\t name : " + name
				+ " ; password : " + password);
	}

	@Test(dataProvider = "admin", dataProviderClass = DataAdmin.class)
	public void testNGTestDataProviderAdmin(int id,String userName,String password,boolean gender) {
		Admin admin = new Admin();
		admin.setUserName(userName);
		admin.setGender(gender);
		admin.setId(id);
		admin.setPassword(password);
		System.out.println(admin != null ? admin.toString() : " admin is null");
	}
}
