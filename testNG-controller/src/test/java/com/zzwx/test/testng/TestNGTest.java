package com.zzwx.test.testng;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author Roger
 * @desc TestNG常规测试
 */
public class TestNGTest {

	/**
	 * 开始测试方法前执行的代码
	 */
	@BeforeTest
	public void testBefore() {
		System.out.println(this.getClassName() + " before!");
	}

	/**
	 * 执行测试方法
	 */
	@Test
	public void testNgTest() {
		System.out.println("hello TestNg");
	}

	/**
	 * 测试方法执行后执行的代码
	 */
	@AfterTest
	public void testAfter() {
		System.out.println(this.getClassName() + " after!");
	}

	public String getClassName() {
		return this.getClass().getSimpleName();
	}
}
