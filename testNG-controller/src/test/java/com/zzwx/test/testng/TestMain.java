package com.zzwx.test.testng;

import org.testng.TestNG;

/**
 * @author Roger
 * @desc 通过Main函数调用TestNG测试方法
 */
public class TestMain {

	public static void main(String[] args) {

		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[]{TestNGTest.class});
		testng.run();

	}

}
