package com.zzwx.test.testng;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author Roger
 * @desc 异常测试
 */
public class TestNGTestException {

	@BeforeTest
	public void before() {
		System.out.println(this.getClassName() + " before!");
	}

	/**
	 * 测试异常,并验证异常信息是否匹配
	 */
	@Test(expectedExceptions = NullPointerException.class, expectedExceptionsMessageRegExp = "this is null point")
	public void testException() {
		throw new NullPointerException("this is null point");
	}

	@AfterTest
	public void after() {
		System.out.println(this.getClassName() + " after!");
	}

	public String getClassName() {
		return this.getClass().getSimpleName();
	}

}
