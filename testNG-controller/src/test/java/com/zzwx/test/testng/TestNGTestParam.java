package com.zzwx.test.testng;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * @author Roger
 * @desc 测试参数化
 */
public class TestNGTestParam {

	public String getClassName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * [Parameters] 数组中的参数名必须和testng.xml中的参数名一致，最好的是顺序都一致，以免有影响
	 * 
	 * @param param0
	 *            此处的参数值与配置文件的参数位置第一个等值
	 * @param param1
	 *            此处的参数值与配置文件的参数位置第二个等值
	 */
	@Test
	@Parameters({ "param0", "param1", "intParam", "boolParam" })
	public void testNGTestParam(String param0, String param1, int intParam,
			boolean boolParam) {
		System.out.println(this.getClassName() + " \t param0 : " + param0
				+ " ; param1 : " + param1 + " ; intParam : " + intParam
				+ " ; boolParam : " + boolParam);
	}

}
