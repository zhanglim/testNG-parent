package com.zzwx.test.shifting;

public class MainRight1 {

	public static void main(String[] args) {

		for (int p = 50; p < 55; p++) {
			System.out.println("////////////算术右移位 - " + p + "/////////////");
			for (int n = 1; n <= 4; n++) {
				System.out.println("需要移位的数字 : " + p + " \t 转换二进制 : "
						+ toBinaryString(p) + " \t 移位量 : " + n
						+ " \t 移位后的二进制 : "
						+ shiftToBinaryRight(toBinaryString(p), n)
						+ " \t value : " + (p >>> n));
			}
			System.out.println("////////////算术右移位 - " + p + "/////////////");
		}
		System.out
				.println("算术右移位 ------>    p >> n = [p / 2的n次方 >= 1 --> 取整]  || [p / 2的n次方 < 1 --> 0]");
	}

	/**
	 * 获取当前数字的二进制
	 * 
	 * @param num
	 *            int数字
	 * @return 二进制字符
	 */
	public static String toBinaryString(int num) {
		return Integer.toBinaryString(num);
	}

	/**
	 * 通过移位量获取移位后的二进制
	 * 
	 * @param binaryStr
	 *            需要移位的二进制
	 * @param shift
	 *            偏移量
	 * @return 移位后的二进制
	 */
	public static String shiftToBinaryRight(String binaryStr, int shift) {

		binaryStr = binaryStr.substring(0, binaryStr.length() - shift) +"."+
				binaryStr.substring(binaryStr.length() - shift, binaryStr.length());
		
		return binaryStr;
	}

}
