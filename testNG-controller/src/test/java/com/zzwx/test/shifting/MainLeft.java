package com.zzwx.test.shifting;

public class MainLeft {

	public static void main(String[] args) {

		for (int p = 1; p < 4; p++) {
			System.out.println("////////////算术左移位 - " + p + "/////////////");
			for (int n = 1; n <= 4; n++) {
				System.out.println("需要移位的数字 : " + p + " \t 转换二进制 : "
						+ toBinaryString(p) + " \t 移位量 : " + n
						+ " \t 移位后的二进制 : "
						+ shiftToBinaryLeft(toBinaryString(p), n)
						+ " \t value : " + (p << n));
			}
			System.out.println("////////////算术左移位 - " + p + "/////////////");
		}
		System.out.println("算术左移位 ------>    p << n = p * 2ⁿ");
	}

	/**
	 * 获取当前数字的二进制
	 * 
	 * @param num
	 *            int数字
	 * @return 二进制字符
	 */
	public static String toBinaryString(int num) {
		return Integer.toBinaryString(num);
	}

	/**
	 * 通过移位量获取移位后的二进制
	 * 
	 * @param binaryStr
	 *            需要移位的二进制
	 * @param shift
	 *            偏移量
	 * @return 移位后的二进制
	 */
	public static String shiftToBinaryLeft(String binaryStr, int shift) {
		for (int i = 0; i < shift; i++) {
			binaryStr += "0";
		}
		return binaryStr;
	}

}
