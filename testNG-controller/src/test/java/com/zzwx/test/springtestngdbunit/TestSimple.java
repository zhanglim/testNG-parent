package com.zzwx.test.springtestngdbunit;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.zzwx.dao.entity.sys.Menu;
import com.zzwx.service.UserService;
import com.zzwx.test.dbunit.DBUnitEachAll;

/**
 * @author Roger
 * @desc testng集成Spring
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestSimple extends AbstractTestNGSpringContextTests{
	
	@Autowired
    private UserService userService;
	
	private DBUnitEachAll dbunit;

	@BeforeTest
	public void beforeTest(){
		dbunit = new DBUnitEachAll();
		dbunit.setUpBackupEachAll("testSimple.xml", "sys_menu");
	}
	
	@Test
	public void testSimple(){
		List<Menu> menus = userService.findMenus();
		System.out.println("菜单列表："+menus);
	}
	
	@AfterTest
	public void afterTest(){
		dbunit.recoverBackupEachAll();
	}
	
}
