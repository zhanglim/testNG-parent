package com.zzwx.dao.dto;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/25.
 */
public class UserAndRoleDTO implements Serializable {
	private static final long serialVersionUID = -4147866916222615287L;

	public UserAndRoleDTO() {
	}

	private int userId;
	private String userName;
	private String roleName;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
