package com.zzwx.dao.mapper.sys;

import com.zzwx.dao.core.MyBatisBaseMapper;
import com.zzwx.dao.dto.UserAndRoleDTO;
import com.zzwx.dao.entity.sys.User;

import java.util.List;

/**
* 代码生成器自动生成
* Date:2016-5-23 14:24:56
* @author
*/
public interface UserMapper extends MyBatisBaseMapper<User> {


    List<UserAndRoleDTO> getUserAndRoles();

}