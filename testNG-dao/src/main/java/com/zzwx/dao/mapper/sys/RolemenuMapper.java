package com.zzwx.dao.mapper.sys;

import com.zzwx.dao.core.MyBatisBaseMapper;
import com.zzwx.dao.entity.sys.Rolemenu;

/**
* 代码生成器自动生成
* Date:2016-5-23 14:24:56
* @author
*/
public interface RolemenuMapper extends MyBatisBaseMapper<Rolemenu> {}