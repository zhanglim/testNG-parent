package com.zzwx.dao.entity.sys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * 代码生成器自动生成
 * Date:2016-5-23 14:24:56
 * @author
 */
@Table(name = "sys_userrole")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Userrole implements Serializable{
	private static final long serialVersionUID = 1L;
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
      @Column(name = "sur_id")
  	  private Integer id; // 用户角色ID
      @Column(name = "sur_roleId")
  	  private Integer roleId; // 角色ID
      @Column(name = "sur_userId")
  	  private Integer userId; // 用户ID
  	  /**
	   * 用户角色ID
	   * @return id
	   */
	  public Integer getId(){
	   return id;
	  }
	  /**
	   * 用户角色ID
	   * @param id 
	   */
	  public void setId(Integer id){
	    this.id = id;
	  }
  	  /**
	   * 角色ID
	   * @return roleId
	   */
	  public Integer getRoleId(){
	   return roleId;
	  }
	  /**
	   * 角色ID
	   * @param roleId 
	   */
	  public void setRoleId(Integer roleId){
	    this.roleId = roleId;
	  }
  	  /**
	   * 用户ID
	   * @return userId
	   */
	  public Integer getUserId(){
	   return userId;
	  }
	  /**
	   * 用户ID
	   * @param userId 
	   */
	  public void setUserId(Integer userId){
	    this.userId = userId;
	  }
}