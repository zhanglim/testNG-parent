package com.zzwx.dao.entity.sys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * 代码生成器自动生成
 * Date:2016-5-23 14:24:56
 * @author
 */
@Table(name = "sys_rolemenu")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rolemenu implements Serializable{
	private static final long serialVersionUID = 1L;
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
      @Column(name = "srm_id")
  	  private Integer id; // 角色菜单ID
      @Column(name = "srm_menuId")
  	  private Integer menuId; // 菜单ID
      @Column(name = "srm_roleId")
  	  private Integer roleId; // 角色ID
  	  /**
	   * 角色菜单ID
	   * @return id
	   */
	  public Integer getId(){
	   return id;
	  }
	  /**
	   * 角色菜单ID
	   * @param id 
	   */
	  public void setId(Integer id){
	    this.id = id;
	  }
  	  /**
	   * 菜单ID
	   * @return menuId
	   */
	  public Integer getMenuId(){
	   return menuId;
	  }
	  /**
	   * 菜单ID
	   * @param menuId 
	   */
	  public void setMenuId(Integer menuId){
	    this.menuId = menuId;
	  }
  	  /**
	   * 角色ID
	   * @return roleId
	   */
	  public Integer getRoleId(){
	   return roleId;
	  }
	  /**
	   * 角色ID
	   * @param roleId 
	   */
	  public void setRoleId(Integer roleId){
	    this.roleId = roleId;
	  }
}