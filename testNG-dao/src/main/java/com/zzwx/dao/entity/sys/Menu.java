package com.zzwx.dao.entity.sys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 代码生成器自动生成 Date:2016-5-23 14:24:56
 * 
 * @author
 */
@Table(name = "sys_menu")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Menu implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sm_id")
	private Integer id; //
	@Column(name = "sm_menuName")
	private String menuName; // 菜单名称
	@Column(name = "sm_menuUrl")
	private String menuUrl; // 菜单URL

	/**
	 * 
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 菜单名称
	 * 
	 * @return menuName
	 */
	public String getMenuName() {
		return menuName;
	}

	/**
	 * 菜单名称
	 * 
	 * @param menuName
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	/**
	 * 菜单URL
	 * 
	 * @return menuUrl
	 */
	public String getMenuUrl() {
		return menuUrl;
	}

	/**
	 * 菜单URL
	 * 
	 * @param menuUrl
	 */
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	@Override
	public String toString() {
		return "Menu [id=" + id + ", menuName=" + menuName + ", menuUrl="
				+ menuUrl + "]";
	}

}