package com.zzwx.dao.entity.sys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * 代码生成器自动生成
 * Date:2016-5-23 14:24:56
 * @author
 */
@Table(name = "sys_user")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
      @Column(name = "su_id")
  	  private Integer id; // 用户ID
      @Column(name = "su_name")
  	  private String name; // 用户名
      @Column(name = "su_password")
  	  private String password; // 用户登陆密码
      @Column(name = "su_status")
  	  private Integer status; // 
  	  /**
	   * 用户ID
	   * @return id
	   */
	  public Integer getId(){
	   return id;
	  }
	  /**
	   * 用户ID
	   * @param id 
	   */
	  public void setId(Integer id){
	    this.id = id;
	  }
  	  /**
	   * 用户名
	   * @return name
	   */
	  public String getName(){
	   return name;
	  }
	  /**
	   * 用户名
	   * @param name 
	   */
	  public void setName(String name){
	    this.name = name;
	  }
  	  /**
	   * 用户登陆密码
	   * @return password
	   */
	  public String getPassword(){
	   return password;
	  }
	  /**
	   * 用户登陆密码
	   * @param password 
	   */
	  public void setPassword(String password){
	    this.password = password;
	  }
  	  /**
	   * 
	   * @return status
	   */
	  public Integer getStatus(){
	   return status;
	  }
	  /**
	   * 
	   * @param status 
	   */
	  public void setStatus(Integer status){
	    this.status = status;
	  }
}