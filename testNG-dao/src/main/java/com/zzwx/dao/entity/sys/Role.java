package com.zzwx.dao.entity.sys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * 代码生成器自动生成
 * Date:2016-5-23 14:24:56
 * @author
 */
@Table(name = "sys_role")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role implements Serializable{
	private static final long serialVersionUID = 1L;
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
      @Column(name = "sr_id")
  	  private Integer id; // 角色ID
      @Column(name = "sr_roleName")
  	  private String roleName; // 角色名称
  	  /**
	   * 角色ID
	   * @return id
	   */
	  public Integer getId(){
	   return id;
	  }
	  /**
	   * 角色ID
	   * @param id 
	   */
	  public void setId(Integer id){
	    this.id = id;
	  }
  	  /**
	   * 角色名称
	   * @return roleName
	   */
	  public String getRoleName(){
	   return roleName;
	  }
	  /**
	   * 角色名称
	   * @param roleName 
	   */
	  public void setRoleName(String roleName){
	    this.roleName = roleName;
	  }
}