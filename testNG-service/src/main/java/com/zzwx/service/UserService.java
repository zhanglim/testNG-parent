package com.zzwx.service;

import com.zzwx.dao.dto.UserAndRoleDTO;
import com.zzwx.dao.entity.sys.Menu;
import com.zzwx.dao.entity.sys.Role;
import com.zzwx.dao.entity.sys.Rolemenu;
import com.zzwx.dao.entity.sys.User;

import java.util.List;
import java.util.Set;


public interface UserService {

    /**
     * 通过用户名获取用户信息
     * @param name 用户名
     * @return User
     */
    User findUserByName(String name);

    /**
     * 获取所有的用户
     * @return List<User>
     */
    List<User> findUsers();

    /**
     * 获取所有菜单
     * @return List<Menu>
     */
    List<Menu> findMenus();
    /**
     * 通过用户名获取该用户所属角色
     * @param name  用户名
     * @return  Role
     */
    Role findRoleByName(String name);

    /**
     * 根据用户ID获取用户所属角色
     * @param userId  用户ID
     * @return Role
     */
    Role findRoleByUserId(int userId);

    /**
     * 根据角色Id获取该角色所属菜单关联信息
     * @param roleId 角色ID
     * @return List<Rolemenu>
     */
    List<Rolemenu> findRolemenusByRoleId(int roleId);

    /**
     * 根据角色ID获取该角色所有菜单
     * @param roleId  角色ID
     * @return  Set<String>
     */
    Set<String> findMenuByRoleId(int roleId);

    /**
     * 通过菜单ID获取菜单详情
     * @param menuId 菜单ID
     * @return Menu
     */
    Menu findMenuById(int menuId);

    /**
     * 获取所有的用户角色信息
     * @return List<UserAndRoleDTO>
     */
    List<UserAndRoleDTO> getUserAndRoles();
}
